function oddCPU() {
    $('#odd').empty();
    $('#odd').append("<h3 style='text-align:center; color:red;'>This is a Straw-Man MVP and is subject to change upon implementation.</h3>" +
        "<div class='row'>" +
        "<div class='col-sm-2'><b>Gateway ID:</b> 1</div>" +
        "<div class='col-sm-6'><b>CPU Tests: </b><br>" +
        "int initialization PASS <br>" +
        "int addition PASS <br>" +
        "int multiplication PASS <br>" +
        "int subtraction PASS <br>" +
        "int division PASS <br>" +
        "int mod PASS <br>" +
        "float initialization PASS <br>" +
        "float addition PASS <br>" +
        "float multiplication PASS <br>" +
        "float subtraction PASS <br>" +
        "float division PASS <br>" +
        "float mod PASS <br>" +
        "</div>" +
        "</div>");
}

function oddMemory() {
    $('#odd').empty();
    $('#odd').append("<h3 style='text-align:center; color:red;'>This is a Straw-Man MVP and is subject to change upon implementation.</h3>" +
        "<div class='row'>" +
        "<div class='col-sm-2'><b>Gateway ID:</b> 1</div>" +
        "<div class='col-sm-2'><b>Memory Test:</b> PASS</div>" +
        "</div>");
}

function sendODD() {
    $.ajax({
        url: 'https://team23.dev.softwareengineeringii.com/api/odd',
        type: 'DELETE',
        crossDomain: true,
        dataType: 'json',
        data: '',
        success: function(response) {
            //old ODD is deleted
            return true;
        },
        error: function (data, status, error) {
            //could not delete old ODD
            return false;
        }
    });

    $.ajax({
        url: 'https://team23.dev.softwareengineeringii.com/api/oddcmd',
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: '',
        success: function(response) {
            //ODD command is sent to Gateway
            $odd.append("<center><p>Waiting for the gateway to respond...</p></center>");

            //Wait for post from Gateway, then display ODD data
            displayODD();
        },
        error: function (data, status, error) {
            //Could not post ODD command
            console.log(data, status, error);
            $odd.append("<center><p>We've encountered an error. Check console log.</p></center>");
        }
    });
}

function displayODD() {
    //check DB every 5 seconds
    var $oddCheck = setInterval(checkODD, 5000);

    if ($oddCheck !== false) {
        clearInterval($oddCheck);

    }
}

function checkODD() {
    $.ajax({
        url: 'https://team23.dev.softwareengineeringii.com/api/odd',
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        data: '',
        success: function(response) {
            //An ODD object was found
            var oddString = JSON.stringify(response, null, 2)
            return oddString;
        },
        error: function (data, status, error) {
            //Not found
            return false;
        }
    });
}