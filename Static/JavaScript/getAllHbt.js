$(function () {
    let timestamp = new Date();
    let $heartbeats = $('#heartbeats');

    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/heartbeat',
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        //data: '',
        success: function(response) {
            let JSONstring = JSON.stringify(response, null, 2);
            console.log("HBT Response: " + JSONstring);
            $.each(JSON.parse(JSONstring), function(idx, heartbeat) {
                let seconds = Math.round(parseFloat(heartbeat.timestamp));
                timestamp.setTime(seconds * 1000);
                let timestampString = timestamp.toLocaleString();

                $heartbeats.append("<div class='row' href='" + heartbeat._id + "'>" +
                    "<div class='col-sm-2' style='background-color:lavender;'><b>Gateway ID:</b> " + heartbeat.gw_uuid + "</div>" +
                    "<div class='col-sm-4' style='background-color:lavenderblush;'><b>Timestamp:</b> " + timestampString + "</div>" +
                    "<div class='col-sm-2' style='background-color:lavender;'><b>Status:</b> " + heartbeat.status + "</div>" +
                    "</div><br>");
            });
        },
        error: function (data, status, error) {
            console.log(data, status, error);
            $heartbeats.append("<li>We've encountered an error. Check console log.</li>");
        }
    });
});