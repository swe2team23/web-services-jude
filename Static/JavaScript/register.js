function registerUser() {
    let radios = document.getElementsByName('types');
    let userType;
    for (let i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked)
        {
            userType = radios[i].value;
            break;
        }
    }
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let passwordv = document.getElementById("passwordv").value;
    let name = document.getElementById("name").value;
    let userID = getUserCount() + 1;

    if (password === passwordv) {
        $.ajax({
            url: 'https://' + window.location.hostname.toString() + '/api/user',
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            data: {"user_id": userID, "full_name": name, "email": email, "password": password, "user_type": userType},
            success: function (response) {
                let JSONstring = JSON.stringify(response, null, 2);
                alert("Registration was successful!");
                window.location.pathname = "/login.html";
            },
            error: function (data, status, error) {
                console.log(data, status, error);
                //error code
            }
        });
    } else {
        alert("Passwords do not match.");
    }
}

function getUserCount() {
    let count = 0;
    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/user',
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        //data: '',
        success: function(response) {
            let JSONstring = JSON.stringify(response, null, 2);
            $.each(JSON.parse(JSONstring), function(idx, user) {
                count += 1;
            });
            return count;
        },
        error: function (data, status, error) {
            console.log(data, status, error);
            return -1;
        }
    });
}