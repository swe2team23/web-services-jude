let timeThreshold = 9999999999999;
let gwID = -1;
let radios = document.getElementsByName('filenames');
let filename = "";
let queue = [];

function submitOdd() {
    timeRequested = new Date();
    timeThreshold = timeRequested.getTime();

    for (let i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked)
        {
            filename = radios[i].value;
            break;
        }
    }

    gwID = document.getElementById('gwID').value;
    console.log("gwID: " + gwID);
    console.log("filename: " + filename);

    putOdd(gwID, filename);
}

function checkTest() {
    $('#oddResponse').empty();

    if (filename === "memory.py") {
        getMemoryTest(gwID, timeThreshold);
    }

    else if (filename === "cpu.py") {
        getCpuTest(gwID, timeThreshold);
    } else {
        $('#oddResponse').append("<h4>No result has been found yet.</h4>\n");
    }
}

function putOdd(gwID, filename) {
    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/odd',
        type: 'PUT',
        crossDomain: true,
        dataType: 'json',
        data: {"gw_uuid" : gwID, "filename" : filename},
        success: function(response) {
            console.log("putOdd() response:");
            console.log(JSON.stringify(response, null, 2));
        },
        error: function (data, status, error) {
            //Could not post ODD command
            console.log(data, status, error);
        }
    });
}

function getOldOdd(gwID) {
    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/odd?gw_uuid=' + gwID,
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        data: '',
        success: function(response) {
            let oldOdd = JSON.stringify(response, null, 2);
            $.each(JSON.parse(oldOdd), function(idx, odd) {
                queue = odd.queue;
            });
        },
        error: function (data, status, error) {
            //Could not post ODD command
            console.log(data, status, error);
        }
    });
}

function getMemoryTest(gwID, timeThreshold) {
    let testTime = new Date();

    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/memoryTest?gw_uuid=' + gwID,
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        data: '',
        success: function(response) {
            let memoryTestJSON = JSON.stringify(response, null, 2);
            $.each(JSON.parse(memoryTestJSON), function(idx, memoryTest) {
                let seconds = Math.round(parseFloat(memoryTest.timestamp));
                testTime.setTime(seconds * 1000);

                if (testTime.getTime() > timeThreshold) {
                    let timestampString = testTime.toLocaleString();

                    $('#oddResponse').append("<div class='row'>" +
                        "<div class='col-sm-2' style=\'background-color:lavender;\'><b>Gateway ID:</b> " + memoryTest.gw_uuid + "</div>" +
                        "<div class='col-sm-4' style='background-color:lavenderblush;'><b>Timestamp:</b> " + timestampString + "</div>" +
                        "<div class='col-sm-2' style='background-color:lavender;'><b>Result:</b> " + memoryTest.result + "</div>" +
                        "</div><br>");

                    putOdd(gwID, "' '");
                }
            });
        },
        error: function (data, status, error) {
            //Could not post ODD command
            console.log(data, status, error);
        }
    });
}

function getCpuTest(gwID, timeThreshold) {
    let testTime = new Date();

    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/cpuTest?gw_uuid=' + gwID,
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        data: '',
        success: function(response) {
            let cpuTestJSON = JSON.stringify(response, null, 2);
            $.each(JSON.parse(cpuTestJSON), function(idx, cpuTest) {
                let seconds = Math.round(parseFloat(cpuTest.timestamp));
                testTime.setTime(seconds * 1000);
                console.log("CpuTest" + idx + " Seconds: " + seconds);
                console.log("testTime.getTime(): " + testTime.getTime());
                console.log("timeRequested.getTime(): " + timeRequested.getTime());

                if (testTime.getTime() > timeThreshold) {
                    let timestampString = testTime.toDateString();

                    $('#oddResponse').append("<h3>CPU Diagnostic Result</h3>\n" +
                        "<div class='row' href='" + cpuTest._id + "'>\n" +
                        "<div class='col-sm-2' style='background-color:lavender;'><b>Gateway ID:</b> " + cpuTest.gw_uuid + "</div>\n" +
                        "<div class='col-sm-4' style='background-color:lavenderblush;'><b>Timestamp:</b> " + timestampString + "</div>\n" +
                        "<div class='col-sm-6' style='background-color:lavender;'><b>CPU Tests: </b><br>\n" +
                        "int initialization: " + cpuTest.int_initialization + " <br>\n" +
                        "int addition: " + cpuTest.int_addition + " <br>\n" +
                        "int multiplication: " + cpuTest.int_multiplication + " <br>\n" +
                        "int subtraction: " + cpuTest.int_subtraction + " <br>\n" +
                        "int division: " + cpuTest.int_division + " <br>\n" +
                        "int mod: " + cpuTest.int_mod + " <br>\n" +
                        "float initialization: " + cpuTest.float_initialization + " <br>\n" +
                        "float addition: " + cpuTest.float_addition + " <br>\n" +
                        "float multiplication: " + cpuTest.float_multiplication + " <br>\n" +
                        "float subtraction: " + cpuTest.float_subtraction + " <br>\n" +
                        "float division: " + cpuTest.float_division + " <br>\n" +
                        "float mod: " + cpuTest.float_mod + " <br>\n" +
                        "</div>\n" +
                        "</div><br>\n");
                }
            });
        },
        error: function (data, status, error) {
            //Could not post ODD command
            console.log(data, status, error);
        }
    });
}