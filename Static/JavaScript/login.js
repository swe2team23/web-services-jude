function login(id, type) {
    '<%Session["userID"] = "' + id + '"; %>';
    '<%Session["userType"] = "' + type + '"; %>';
    console.log('<%= Session["userType"] %>');

    window.location.pathname = "/dashboard.html";
}

function getUserLogin() {
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let userID = -1;
    let userType = "none";
    let success = 0;

    $.ajax({
        url: 'https://' + window.location.hostname.toString() + '/api/user',
        type: 'GET',
        crossDomain: true,
        dataType: 'json',
        //data: '',
        success: function(response) {
            let JSONstring = JSON.stringify(response, null, 2);
            $.each(JSON.parse(JSONstring), function(idx, user) {
                console.log(user);
                if (user.email === email && user.password === password) {
                    success = 1;
                    userID = user.user_id;
                    userType = user.user_type;
                }
            });
            if (success === 1) {
                console.log("REQ SUCCESS \n-ID: " + userID + " \n-Email : " + email + " \n-Password: " + password + " \n-Type: " + userType);
                login(userID, userType);
            } else {
                //could not login
                console.log("No match found :(");
            }
        },
        error: function (data, status, error) {
            console.log(data, status, error);
            //error code
        }
    });
}